import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import Ship from './Ship';

// The data prop, which is provided by the HOC below contains
// a `loading` key while the query is in flight
function RequestList (props) {
  const { data: { freightQuoteRequests, loading } } = props;
  if (loading) {
    return <div>Loading</div>;
  }

  return (
    <section>
      <h1>Requests</h1>
      <ul>
        {freightQuoteRequests && freightQuoteRequests.map((request, index) => (
          <li key={index}>
            <div>
              <h2>Request {index + 1}. </h2>
                Amount {request.amount}<br />
                Containers {request.containers}<br />
                Origin {request.origin.name}<br />
                Destination {request.destination.name}<br />
                Type {request.type}<br />
              <h3>Matches</h3><br />
              {request.quoteRequestAndShipMatches &&
                  request.quoteRequestAndShipMatches.map((match, index) => (
                    <div key={index}>
                      Capacity {match.capacityMatch}<br />
                      Distance {match.distanceMatch}<br />
                      Time {match.timeMatch}<br />
                      {Ship(match.ship, 0)}
                    </div>
                  ))}
              <h3>Quotes</h3><br />
              {request.freightQuotes && request.freightQuotes.map((
                    quote,
                    index
                  ) => (
                    <div key={index}>
                      Quote id: {quote.id}<br />
                      Estimated Delivery {quote.estimatedDeliveryStart}<br />
                      Estimated Delivery {quote.estimatedDeliveryEnd}<br />
                      Estimated Pickup {quote.estimatedPickupStart}<br />
                      Estimated Pickup {quote.estimatedPickupEnd}<br />
                      Price {quote.price}<br />
                      Terms {quote.terms}<br />
                      Timelimit {quote.timelimit}<br />
                      Duration Minutes {quote.durationMinutes}<br />
                      {Ship(quote.ship, 0)}

                      <h4>Counter offers</h4><br />
                      {quote.counterOffers && quote.counterOffers.map((
                        counterOffer,
                        index
                      ) => (
                        <div key={index}>
                          Counter offer id: {counterOffer.id}<br />
                          Price {counterOffer.price}<br />
                          Terms {counterOffer.terms}<br />
                        </div>
                      ))}
                    </div>
                  ))}
            </div>
          </li>
          ))}
      </ul>
      <style jsx>
        {
          `
        section {
          padding-bottom: 20px;
        }
        li {
          display: block;
          margin-bottom: 10px;
        }
        div {
          align-items: center;
        }
        a {
          font-size: 14px;
          margin-right: 10px;
          text-decoration: none;
          padding-bottom: 0;
          border: 0;
        }
        span {
          font-size: 14px;
          margin-right: 5px;
        }
        ul {
          margin: 0;
          padding: 0;
        }
        button:before {
          align-self: center;
          border-style: solid;
          border-width: 6px 4px 0 4px;
          border-color: #ffffff transparent transparent transparent;
          content: "";
          height: 0;
          width: 0;
        }
      `
        }
      </style>
    </section>
  );
}

const allRequests = gql`
    {
        freightQuoteRequests {
            id
            amount
            containers
            origin {
                name
            }
            destination {
                name
            }
            type
            quoteRequestAndShipMatches {
                id
                capacityMatch
                distanceMatch
                timeMatch
                ship {
                    id
                    name
                    openPositionLocation
                    openPositionTime
                    maxFull
                    maxEmpty
                    maxContainers
                }
            }
            freightQuotes {
                id
                price
                terms
                timelimit
                durationMinutes
                ship {
                    id
                    name
                    openPositionLocation
                    openPositionTime
                    maxFull
                    maxEmpty
                    maxContainers
                }
                counterOffers {
                    id
                    price
                    terms
                }
            }
        }
    }
`;

// The `graphql` wrapper executes a GraphQL query and makes the results
// available on the `data` prop of the wrapped component (RequestList here)
export default graphql(allRequests, {
  options: ownProps => ({ variables: {} }),
  props: ({ data }) => ({ data })
})(RequestList);
