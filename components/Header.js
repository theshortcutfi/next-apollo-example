import Link from 'next/prefetch';

// TODO Clearer who you are, Ship Operator or Cargo owner
export default ({ pathname }) => (
  <header>
    <Link href='/'>
      <a className={pathname === '/' && 'is-active'}>Ships</a>
    </Link>

    <Link href='/requests'>
      <a className={pathname === '/requests' && 'is-active'}>Requests</a>
    </Link>

    <Link href='/about'>
      <a className={pathname === '/about' && 'is-active'}>About</a>
    </Link>

    <Link href='https://www.theshortcut.fi/graphiql'>
      <a>GraphiQL</a>
    </Link>

    <style jsx>
      {
        `
      header {
        margin-bottom: 25px;
      }
      a {
        font-size: 14px;
        margin-right: 15px;
        text-decoration: none;
      }
      .is-active {
        text-decoration: underline;
      }
    `
      }
    </style>
  </header>
);
