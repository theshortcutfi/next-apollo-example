export default function PartialCargoSpace ({ space, index }) {
  return (
    <div>
      <h4>Partial cargo space</h4>
      <span>{index + 1}. </span>
      Tonnes {space.tonnes}<br />
      Length {space.length}<br />
      Width {space.width}<br />
      Height {space.height}<br />
      Recurrence In Days {space.recurrenceInDays}<br />
      Origin {space.origin.name}<br />
      Destination {space.destination.name}
    </div>
  );
}
