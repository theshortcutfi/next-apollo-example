import App from '../components/App';
import Header from '../components/Header';
import RequestList from '../components/RequestList';
import InputCargo from '../components/InputCargo';
import InputOffer from '../components/InputOffer';
import InputCounterOffer from '../components/InputCounterOffer';
import withData from '../lib/withData';

// TODO Display of price and time validity on matches page if offer is received, if not offer received display reminder/ping button. "Last page" is only display of confirmed historical matches
// TODO Negotiation possibility within the platform ( counter offer: price, terms ; new offer: price, terms, timelimit)
// TODO Automatic sending of emails and notifications. ping ship operator as extra request for offer (backend)
export default withData(props => (
  <App>
    <Header pathname={props.url.pathname} />
    <InputCargo />
    <InputOffer />
    <InputCounterOffer />
    <RequestList />
  </App>
));
