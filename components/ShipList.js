import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import Ship from './Ship';
import PartialCargoSpace from './PartialCargoSpace';
import InputOpenPosition from './InputOpenPosition';
import InputPartialCargoSpace from './InputPartialCargoSpace';

// The data prop, which is provided by the HOC below contains
// a `loading` key while the query is in flight
function ShipList (props) {
  const { data: { ships, loading } } = props;
  if (loading) {
    return <div>Loading</div>;
  }

  // TODO Specs of vessels (age owner....)
  return (
    <section>
      <h1>Ships</h1>
      <ul>
        {ships && ships.map((ship, index) => (
          <li key={ship.id}>
            {Ship(ship)}
            <InputOpenPosition shipId={ship.id} />
            <InputPartialCargoSpace shipId={ship.id} />
            {ship.partialCargoSpaces &&
                ship.partialCargoSpaces.map((space, index) => (
                  <PartialCargoSpace space={space} key={index} index={index} />
                ))}
          </li>
          ))}
      </ul>
      <style jsx>
        {
          `
        section {
          padding-bottom: 20px;
        }
        li {
          display: block;
          margin-bottom: 10px;
        }
        div {
          align-items: center;
          display: flex;
        }
        a {
          font-size: 14px;
          margin-right: 10px;
          text-decoration: none;
          padding-bottom: 0;
          border: 0;
        }
        span {
          font-size: 14px;
          margin-right: 5px;
        }
        ul {
          margin: 0;
          padding: 0;
        }
        button:before {
          align-self: center;
          border-style: solid;
          border-width: 6px 4px 0 4px;
          border-color: #ffffff transparent transparent transparent;
          content: "";
          height: 0;
          width: 0;
        }
      `
        }
      </style>
    </section>
  );
}

const allShips = gql`
    {
        ships {
            id
            name
            openPositionLocation
            openPositionTime
            position
            longitude
            latitude
            maxFull
            maxEmpty
            maxContainers
            quoteRequestAndShipMatches {
                capacityMatch
                distanceMatch
                timeMatch
                freightQuoteRequest {
                    amount
                    comission
                    origin {
                        name
                    }
                    destination {
                        name
                    }
                    type
                    body
                }
            }
            partialCargoSpaces {
                tonnes
                length
                width
                height
                recurrenceInDays
                origin {
                    name
                }
                destination {
                    name
                }
            }
        }
    }
`;

// The `graphql` wrapper executes a GraphQL query and makes the results
// available on the `data` prop of the wrapped component (ShipList here)
export default graphql(allShips, {
  options: ownProps => ({ variables: {} }),
  props: ({ data }) => ({ data })
})(ShipList);
