import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

// TODO Input offer (price, terms, time validity (10min, 15, 20, 30, 45, 1hr, 2hrs, 3hrs)) default to be set in the user settings but possible to edit with slider or drop down in UI
// Standard T&C, Harbourtime (Loading/Unloading time), Geographical deviation clause NOTE! Will be solved with a free text field in MVP
// Price (Lumpsum/Per unit/Per ton)
function InputOffer ({ makeOffer }) {
  function handleSubmit (e) {
    e.preventDefault();
    let elements = e.target.elements;
    let ship = elements.ship.value;
    let price = elements.price.value;
    let request = elements.request.value;
    let terms = elements.terms.value;
    let estimatedPickup = elements.estimatedPickup.value;
    let estimatedDelivery = elements.estimatedDelivery.value;
    let timeLimitDate = elements.timeLimitDate.value;
    let timeLimitTime = elements.timeLimitTime.value;
    let durationMinutes = elements.durationMinutes.value;
    if (
      request === '' ||
        ship === '' ||
        price === '' ||
        terms === '' ||
        !estimatedPickup ||
        !estimatedDelivery ||
        !timeLimitDate ||
        !timeLimitTime ||
        !durationMinutes
    ) {
      window.alert('All fields are required.');
      return false;
    }
    let timeLimit = new Date(`${timeLimitDate} ${timeLimitTime}`);
    makeOffer(
      request,
      ship,
      +price,
      terms,
      `${estimatedPickup} 00:00:00`,
      `${estimatedDelivery} 00:00:00`,
      timeLimit,
      +durationMinutes
    );

    // reset form
    elements.request.value = '';
    elements.ship.value = '';
    elements.price.value = '';
    elements.terms.value = '';
    elements.estimatedPickup.value = '';
    elements.estimatedDelivery.value = '';
    elements.timeLimitDate.value = '';
    elements.timeLimitTime.value = '';
    elements.durationMinutes.value = '';
  }

  return (
    <form onSubmit={handleSubmit}>
      <h1>Make offer</h1>
      <label>Request<input placeholder='Request id' name='request' /></label>
      <label>Ship<input placeholder='Ship id' name='ship' /></label>
      <label>
        Price<input placeholder='Price' name='price' type='number' />
      </label>
      <label>
        T&C
        <input placeholder='Terms&amp;Conditions' name='terms' type='string' />
      </label>
      <label>
        Estimated pickup<input name='estimatedPickup' type='date' />
      </label>
      <label>
        Estimated delivery<input name='estimatedDelivery' type='date' />
      </label>
      <label>
        Offer time-limit date<input name='timeLimitDate' type='date' />
      </label>
      <label>
        Offer time-limit time<input name='timeLimitTime' type='time' />
      </label>
      <label>
        Offer duration (minutes)<input name='durationMinutes' type='number' />
      </label>
      <button type='submit'>Submit</button>
      <style jsx>
        {
          `
        form {
          border-bottom: 1px solid #ececec;
          padding-bottom: 20px;
          margin-bottom: 20px;
        }
        h1 {
          font-size: 20px;
        }
        input {
          display: block;
          margin-bottom: 10px;
        }
      `
        }
      </style>
    </form>
  );
}

const makeOffer = gql`
  mutation makeOffer($request_id: ID!, $ship_id: ID!, $price: Int!, $terms: String!, $estimatedPickup: Time!, $estimatedDelivery: Time!, $timeLimit: Time!, $durationMinutes: Int!) {
    makeOffer(freightQuoteRequest: $request_id, ship: $ship_id, price: $price, terms: $terms, estimatedPickup: $estimatedPickup, estimatedDelivery: $estimatedDelivery, timeLimit: $timeLimit, durationMinutes: $durationMinutes) {
      id
    }
  }
`;

export default graphql(makeOffer, {
  props: ({ mutate }) => ({
    makeOffer: (
      requestId,
      shipId,
      price,
      terms,
      estimatedPickup,
      estimatedDelivery,
      timeLimit,
      durationMinutes
    ) =>
      mutate({
        variables: {
          request_id: requestId,
          ship_id: shipId,
          price,
          terms,
          estimatedPickup,
          estimatedDelivery,
          timeLimit,
          durationMinutes
        },
        updateQueries: {
          allCounterOffers: (previousResult, { mutationResult }) => {
            const newQuote = mutationResult.data.makeOffer;
            return Object.assign({}, previousResult, {
              // Append the new quote
              allCounterOffers: [newQuote, ...previousResult.allCounterOffers]
            });
          }
        }
      })
  })
})(InputOffer);
