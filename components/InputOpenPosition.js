// TODO Input Open Position (Ship, Port name, Date)
// Laycan (päivämäärä match)
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

function InputOpenPosition ({ updateOpenPosition, shipId }) {
  function handleSubmit (e) {
    e.preventDefault();
    let elements = e.target.elements;
    let date = elements.date.value;
    let port = elements.port.value;
    let shipId = elements.shipId.value;
    if (!date || port === '' || shipId === '') {
      window.alert('All fields are required.');
      return false;
    }
    updateOpenPosition({ date, port, shipId });

    // reset form
    elements.date.value = '';
    elements.port.value = '';
    elements.shipId.value = '';
  }

  return (
    <form onSubmit={handleSubmit}>
      <h3>Update open position</h3>
      <label>Date<input placeholder='Date' name='date' type='date' /></label>
      <label>Port name<input placeholder='Port name' name='port' /></label>
      <label>
        Ship id<input placeholder='Ship id' name='shipId' value={shipId} />
      </label>
      <button type='submit'>Submit</button>
      <style jsx>
        {
          `
        form {
          border-bottom: 1px solid #ececec;
          padding-bottom: 20px;
          margin-bottom: 20px;
        }
        h1 {
          font-size: 20px;
        }
        input {
          display: block;
          margin-bottom: 10px;
        }
      `
        }
      </style>
    </form>
  );
}

const updateOpenPosition = gql`
    mutation updateOpenPosition($date: String, $port: String, $shipId: ID) {
        updateOpenPosition(date: $date, port: $port, shipId: $shipId) {
            id
            openPositionLocation
            openPositionTime
        }
    }
`;

export default graphql(updateOpenPosition, {
  props: ({ mutate }) => ({
    updateOpenPosition: ({ date, port, shipId }) =>
      mutate({ variables: { date, port, shipId } })
  })
})(InputOpenPosition);
