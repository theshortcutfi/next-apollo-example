// TODO Input & Edit of cargo (Port of loading, Port of discharge, Earliest pickup, Latest delivery, Amount, Commodity name, Type of Packing, Shipping details and terms & conditions if needed)
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

function InputCargo ({ makeRequest }) {
  let fields = {
    amount: 'number',
    availableSpace: 'checkbox',
    body: 'string',
    comission: 'string',
    commodityName: 'string',
    containerType: 'string',
    containers: 'number',
    destination: 'string',
    earliestPickup: 'datetime-local',
    email: 'string',
    harborTimes: 'string',
    height: 'string',
    imoClass: 'string',
    incoTerms: 'string',
    latestDelivery: 'datetime-local',
    lengths: 'string',
    loading: 'string',
    origin: 'string',
    packingType: 'string',
    shippingDetails: 'string',
    stackable: 'checkbox',
    stowage: 'string',
    surface: 'string',
    terms: 'string',
    type: 'string',
    volume: 'string',
    width: 'string'
  };

  const mapValue = {
    number: x => +x.value,
    checkbox: x => x.checked,
    'datetime-local': x => `${x.value}:00`
  };

  function handleSubmit (e) {
    e.preventDefault();
    let elements = e.target.elements;
    let values = {};
    for (let field of Object.keys(fields)) {
      const element = elements[field];
      const type = fields[field];
      const value = element.value;
      if (!value) {
        window.alert('All fields are required.');
        return false;
      }
      const map = mapValue[type];
      values[field] = map ? map(element) : value;
    }
    makeRequest(values);
    // reset form
    for (let field of Object.keys(fields)) {
      elements[field].value = '';
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <h1>Make freight quote request</h1>
      {Object.keys(fields).map((field, index) => {
        let name = field[0].toUpperCase() + field.substring(1);
        return (
          <label key={index}>
            {name}<input placeholder={name} name={field} type={fields[field]} />
          </label>
        );
      })}
      <button type='submit'>Submit</button>
      <style jsx>
        {
          `
        form {
          border-bottom: 1px solid #ececec;
          padding-bottom: 20px;
          margin-bottom: 20px;
        }
        h1 {
          font-size: 20px;
        }
        input {
          display: block;
          margin-bottom: 10px;
        }
      `
        }
      </style>
    </form>
  );
}

const makeRequest = gql`
    mutation makeRequest($amount: Int, $availableSpace: Boolean, $body: String, $comission: String, $commodityName: String, $containerType: String, $containers: Int, $destination: ID, $earliestPickup: Time, $email: String, $harborTimes: String, $height: String, $imoClass: String, $incoTerms: String, $latestDelivery: Time, $lengths: String, $loading: String, $origin: ID, $packingType: String, $shippingDetails: String, $stackable: Boolean, $stowage: String, $surface: String, $terms: String, $type: String, $volume: String, $width: String) {
        makeRequest(amount: $amount, availableSpace: $availableSpace, body: $body, comission: $comission, commodityName: $commodityName, containerType: $containerType, containers: $containers, destination: $destination, earliestPickup: $earliestPickup, email: $email, harborTimes: $harborTimes, height: $height, imoClass: $imoClass, incoTerms: $incoTerms, latestDelivery: $latestDelivery, lengths: $lengths, loading: $loading, origin: $origin, packingType: $packingType, shippingDetails: $shippingDetails, stackable: $stackable, stowage: $stowage, surface: $surface, terms: $terms, type: $type, volume: $volume, width: $width) {
            id
        }
    }
`;

export default graphql(makeRequest, {
  props: ({ mutate }) => ({
    makeRequest: variables => mutate({
      variables: variables,
      updateQueries: {
        allRequests: (previousResult, { mutationResult }) => {
          const newRequest = mutationResult.data.makeRequest;
          return Object.assign({}, previousResult, {
            // Append the new quote
            allRequests: [newRequest, ...previousResult.allRequests]
          });
        }
      }
    })
  })
})(InputCargo);
