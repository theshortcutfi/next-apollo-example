// TODO Input of Part(ial) Cargo Space (Tonnes, l x w x h, schedule [ origin, destination, earliest pickup, latest delivery, recurrence (every n days/weeks/months)])

import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

function InputPartialCargoSpace ({ addPartialCargoSpace, shipId }) {
  function handleSubmit (e) {
    e.preventDefault();
    let elements = e.target.elements;
    let origin = elements.origin.value;
    let destination = elements.destination.value;
    let earliestPickup = elements.earliestPickup.value;
    let latestDelivery = elements.latestDelivery.value;
    let recurrenceInDays = +elements.recurrenceInDays.value;
    let tonnes = +elements.tonnes.value;
    let lengths = +elements.lengths.value;
    let width = +elements.width.value;
    let height = +elements.height.value;
    let ship = elements.ship.value;
    if (
      !origin ||
        !destination ||
        !earliestPickup ||
        !latestDelivery ||
        !recurrenceInDays ||
        !tonnes ||
        !lengths ||
        !width ||
        !height ||
        !ship
    ) {
      window.alert('All fields are required.');
      return false;
    }
    earliestPickup = `${earliestPickup} 00:00:00`;
    latestDelivery = `${latestDelivery} 00:00:00`;
    addPartialCargoSpace({
      origin,
      destination,
      earliestPickup,
      latestDelivery,
      recurrenceInDays,
      tonnes,
      lengths,
      width,
      height,
      ship
    });

    // reset form
    elements.origin.value = '';
    elements.destination.value = '';
    elements.earliestPickup.value = '';
    elements.latestDelivery.value = '';
    elements.recurrenceInDays.value = '';
    elements.tonnes.value = '';
    elements.lengths.value = '';
    elements.width.value = '';
    elements.height.value = '';
    elements.ship.value = '';
  }

  return (
    <form onSubmit={handleSubmit}>
      <h3>Add partial cargo space</h3>
      <label>Origin<input placeholder='Origin' name='origin' /></label>
      <label>
        Destination<input placeholder='Destination' name='destination' />
      </label>
      <label>
        Earliest Pickup

        <input
          placeholder='Earliest Pickup'
          name='earliestPickup'
          type='date'
        />
      </label>
      <label>
        Latest Delivery

        <input
          placeholder='Latest Delivery'
          name='latestDelivery'
          type='date'
        />
      </label>
      <label>
        Recurrence in days
        <input
          placeholder='Recurrence in days'
          name='recurrenceInDays'
          type='number'
        />
      </label>
      <label>
        Tonnes<input placeholder='Tonnes' name='tonnes' type='number' />
      </label>
      <label>
        Length<input placeholder='Length' name='lengths' type='number' />
      </label>
      <label>
        Width<input placeholder='Width' name='width' type='number' />
      </label>
      <label>
        Height<input placeholder='Height' name='height' type='number' />
      </label>
      <label>ship<input placeholder='ship' name='ship' value={shipId} /></label>
      <button type='submit'>Submit</button>
      <style jsx>
        {
          `
        form {
          border-bottom: 1px solid #ececec;
          padding-bottom: 20px;
          margin-bottom: 20px;
        }
        h1 {
          font-size: 20px;
        }
        input {
          display: block;
          margin-bottom: 10px;
        }
      `
        }
      </style>
    </form>
  );
}

const addPartialCargoSpace = gql`
    mutation addPartialCargoSpace($destination: ID, $earliestPickup: Time, $height: Int, $latestDelivery: Time, $lengths: Int, $origin: ID, $ship: ID, $tonnes: Int, $width: Int, $recurrenceInDays: Int) {
        addPartialCargoSpace(destination: $destination, earliestPickup: $earliestPickup, height: $height, latestDelivery: $latestDelivery, lengths: $lengths, origin: $origin, ship: $ship, tonnes: $tonnes, width: $width, recurrenceInDays: $recurrenceInDays) {
            id
        }
    }
`;

export default graphql(addPartialCargoSpace, {
  props: ({ mutate }) => ({
    addPartialCargoSpace: (
      {
        origin,
        destination,
        earliestPickup,
        latestDelivery,
        recurrenceInDays,
        tonnes,
        lengths,
        width,
        height,
        ship
      }
    ) =>
      mutate({
        variables: {
          origin,
          destination,
          earliestPickup,
          latestDelivery,
          recurrenceInDays,
          tonnes,
          lengths,
          width,
          height,
          ship
        },
        updateQueries: {
          allPartialCargoSpaces: (previousResult, { mutationResult }) => {
            const newPartialCargoSpace = mutationResult.data.addPartialCargoSpace;
            return Object.assign({}, previousResult, {
              // Append the new quote
              allPartialCargoSpaces: [
                newPartialCargoSpace,
                ...previousResult.allPartialCargoSpaces
              ]
            });
          }
        }
      })
  })
})(InputPartialCargoSpace);
