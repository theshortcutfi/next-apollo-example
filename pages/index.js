import App from '../components/App';
import Header from '../components/Header';
import ShipList from '../components/ShipList';
import withData from '../lib/withData';

// TODO Page after Login (Where Ships/Cargo history available), alternatively button on best matches page
export default withData(props => (
  <App>
    <Header pathname={props.url.pathname} />
    <ShipList />
  </App>
));
