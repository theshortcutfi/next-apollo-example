// TODO Negotiation possibility within the platform ( counter offer: price, terms ; new offer: price, terms, timelimit)
// Price (Lumpsum/Per unit/Per ton)
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

function InputCounterOffer ({ makeCounterOffer }) {
  function handleSubmit (e) {
    e.preventDefault();
    let elements = e.target.elements;
    let ship = elements.ship.value;
    let price = elements.price.value;
    let freightQuoteRequest = elements.request.value;
    let freightQuote = elements.quote.value;
    let terms = elements.terms.value;
    if (
      freightQuoteRequest === '' ||
        ship === '' ||
        price === '' ||
        terms === '' ||
        freightQuote === ''
    ) {
      window.alert('All fields are required.');
      return false;
    }
    price = +price;
    makeCounterOffer({ freightQuoteRequest, ship, price, terms, freightQuote });

    // reset form
    elements.request.value = '';
    elements.ship.value = '';
    elements.price.value = '';
    elements.terms.value = '';
    elements.quote.value = '';
  }

  return (
    <form onSubmit={handleSubmit}>
      <h1>Make counter offer</h1>
      <label>Request<input placeholder='Request id' name='request' /></label>
      <label>Ship<input placeholder='Ship id' name='ship' /></label>
      <label>Quote<input placeholder='Quote id' name='quote' /></label>
      <label>
        Price<input placeholder='Price' name='price' type='number' />
      </label>
      <label>
        T&C
        <input placeholder='Terms&amp;Conditions' name='terms' type='string' />
      </label>
      <button type='submit'>Submit</button>
      <style jsx>
        {
          `
        form {
          border-bottom: 1px solid #ececec;
          padding-bottom: 20px;
          margin-bottom: 20px;
        }
        h1 {
          font-size: 20px;
        }
        input {
          display: block;
          margin-bottom: 10px;
        }
      `
        }
      </style>
    </form>
  );
}

const makeCounterOffer = gql`
    mutation makeCounterOffer($freightQuote: ID, $freightQuoteRequest: ID, $price: Int, $ship: ID, $terms: String) {
        makeCounterOffer(freightQuote: $freightQuote, freightQuoteRequest: $freightQuoteRequest, price: $price, ship: $ship, terms: $terms) {
            id
        }
    }
`;

export default graphql(makeCounterOffer, {
  props: ({ mutate }) => ({
    makeCounterOffer: (
      { freightQuote, freightQuoteRequest, price, ship, terms }
    ) =>
      mutate({
        variables: { freightQuote, freightQuoteRequest, price, ship, terms },
        updateQueries: {
          allCounterOffers: (previousResult, { mutationResult }) => {
            const newCounterOffer = mutationResult.data.makeCounterOffer;
            return Object.assign({}, previousResult, {
              // Append the new quote
              allCounterOffers: [
                newCounterOffer,
                ...previousResult.allCounterOffers
              ]
            });
          }
        }
      })
  })
})(InputCounterOffer);
