// TODO Display of price and time validity on matches page if offer is received, if not offer received display reminder/ping button. "Last page" is only display of confirmed historical matchesi

import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import Ship from './Ship';

// The data prop, which is provided by the HOC below contains
// a `loading` key while the query is in flight
function MatchList (props) {
  const { data: { matches, loading } } = props;
  if (loading) {
    return <div>Loading</div>;
  }

  return (
    <section>
      <h1>Matches</h1>
      <ul>
        {matches && matches.map((match, index) => {
          let request = match.request;
          return (
            <li key={index}>
              <div>
                <span>{index + 1}. </span>
                  Capacity {match.capacityMatch}<br />
                  Distance {match.distanceMatch}<br />
                  Time {match.timeMatch}<br />
                  Amount {request.amount}<br />
                  Containers {request.containers}<br />
                  Origin {request.origin.name}<br />
                  Destination {request.destination.name}<br />
                  Type {request.type}<br />
                {Ship(match.ship, 0)}
              </div>
            </li>
          );
        })}
      </ul>
      <style jsx>
        {
          `
        section {
          padding-bottom: 20px;
        }
        li {
          display: block;
          margin-bottom: 10px;
        }
        div {
          align-items: center;
          display: flex;
        }
        a {
          font-size: 14px;
          margin-right: 10px;
          text-decoration: none;
          padding-bottom: 0;
          border: 0;
        }
        span {
          font-size: 14px;
          margin-right: 5px;
        }
        ul {
          margin: 0;
          padding: 0;
        }
        button:before {
          align-self: center;
          border-style: solid;
          border-width: 6px 4px 0 4px;
          border-color: #ffffff transparent transparent transparent;
          content: "";
          height: 0;
          width: 0;
        }
      `
        }
      </style>
    </section>
  );
}

const allMatches = gql`
    {
        quoteRequestAndShipMatches {
            capacityMatch
            distanceMatch
            timeMatch
            freightQuoteRequest {
                amount
                comission
                origin {
                    name
                }
                destination {
                    name
                }
                type
                body
            }
            ship {
                name
                openPositionLocation
                openPositionTime
                position
                longitude
                latitude
                maxFull
                maxEmpty
                maxContainers
            }
        }
    }
`;

// The `graphql` wrapper executes a GraphQL query and makes the results
// available on the `data` prop of the wrapped component (ShipList here)
export default graphql(allMatches, {
  options: ownProps => ({ variables: {} }),
  props: ({ data }) => ({ data })
})(MatchList);
