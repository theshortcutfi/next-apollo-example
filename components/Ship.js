export default function Ship (ship) {
  return (
    <div>
      <h2>{ship.name}</h2>
      id {ship.id}<br />
      position {ship.position}<br />
      longitude {ship.longitude}<br />
      latitude {ship.latitude}<br />
      maxFull {ship.maxFull}<br />
      maxEmpty {ship.maxEmpty}<br />
      maxContainers {ship.maxContainers}<br />
      openPositionLocation {ship.openPositionLocation}<br />
      openPositionTime {ship.openPositionTime}
    </div>
  );
}
